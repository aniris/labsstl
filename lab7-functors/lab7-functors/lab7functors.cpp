// 1.	Разработать функтор, позволяющий собирать статистику о последовательности целых чисел (послед может содержать числа от -500 до 500). Функтор после обработки последовательности алгоритмом for_each должен предоставлять следующую статистику:
//a.Максимальное число в последовательности
//b.Минимальное число в последовательности
//c.Среднее число в последовательности
//d.Количество положительных чисел
//e.Количество отрицательных чисел
//f.Сумму нечетных элементов последовательности
//g.Сумму четных элементов последовательности
//h.Совпадают ли первый и последний элементы последовательности.
//
//Проверить работу программы на случайно сгенерированных последовательностях.

//

#include "stdafx.h"
#include <iostream>
#include "Numbers.h"
#include <algorithm>
#include <random>
#include <time.h>

using namespace std;

void Print(int value) {
	cout << value << " ";
}

int Random_() {
	return rand() % 1000 - 500;
}

int main()
{
	vector<int> v(5), v2(6);
	Numbers f;
	srand(time(0));
	generate(v.begin(), v.end(), Random_);
	generate(v2.begin(), v2.end(), Random_);

	f = for_each(v.begin(), v.end(), f);
	f = for_each(v2.begin(), v2.end(), f);
	cout << "Vector:" << endl;
	for_each(v.begin(), v.end(), Print);
	cout << endl << endl;
	cout << "Vector2:" << endl;
	for_each(v2.begin(), v2.end(), Print);
	cout << endl << endl;
	f();

    return 0;
}

