//1.����������� �������, ����������� �������� ���������� � ������������������ ����� �����(������ ����� ��������� ����� �� - 500 �� 500).������� ����� ��������� ������������������ ���������� for_each ������ ������������� ��������� ���������� :
//+a.������������ ����� � ������������������
//+b.����������� ����� � ������������������
//+c.������� ����� � ������������������
//+d.���������� ������������� �����
//+e.���������� ������������� �����
//+f.����� �������� ��������� ������������������
//+g.����� ������ ��������� ������������������
//h.��������� �� ������ � ��������� �������� ������������������.
//
//��������� ������ ��������� �� �������� ��������������� �������������������.

#pragma once
#include <vector>
#include <iostream>

using namespace std;

class Numbers {
private:
	int max;
	int min;
	float average = 0;
	int countPos = 0;
	int countNeg = 0;
	int sumEven = 0;
	int sumOdd = 0;
	bool equalFisrtLast;
	int countNumber;
	int first = 0;
	int last = 0;
	int sum = 0;

public:
	Numbers() :countNumber(0) {};

	void operator() (int value) {
		countNumber++;

		if (countNumber == 1) {
			max = value;
			min = value;
			first = value;
		}

		if (max < value) max = value;
		if (min > value) min = value;

		if (value % 2 == 0) sumEven += value;
		else sumOdd += value;

		if (value >= 0) countPos++;
		else countNeg++;

		if (first == value) equalFisrtLast = true;
		else equalFisrtLast = false;
		last = value;

		sum += value;
	}

	

	void operator() () {
		if (countNumber == 0) cout << "Null" << endl;
		else {
			average = float(sum) / countNumber;
			cout << "Max: " << max << endl;
			cout << "Min: " << min << endl;
			cout << "Average: " << average << endl;
			cout << "Positive: " << countPos << endl;
			cout << "Negative: " << countNeg << endl;
			cout << "Sum even: " << sumEven << endl;
			cout << "Sum odd: " << sumOdd << endl;
			cout << "First == Last? " << (equalFisrtLast ? "true" : "false") << endl << endl;
		}
	}
};
