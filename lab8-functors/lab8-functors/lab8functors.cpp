// lab8functors.cpp: определяет точку входа для консольного приложения.
//1.Разработать программу, которая, используя только стандартные алгоритмы и функторы, умножает каждый элемент списка чисел с плавающей  точкой на число PI
//2.a.Реализует иерархию геометрических фигур состоящую из :
//i.Класс Shape.
//1.	Содержит информацию о положении центра фигуры(координаты x и y).
//2.	Содердит метод IsMoreLeft, позволяющий определить расположена ли данная фигура левее(определяется по положению центра) чем фигура переданная в качестве аргумента
//3.	Содердит метод IsUpper, позволяющий определить расположена ли данная фигура выше(определяется по положению центра) чем фигура переданная в качестве аргумента
//4.	Определяет чисто виртаульную функцию рисования Draw(каждая фигура в реализации этой функци должна выводить на стандартный вывод свое название и положение цента)
//ii.Класс Circle производный от класса Shape
//1.	Реализует Draw
//iii.Класс Triangle производный от класса Shape
//1.	Реализует Draw
//iv.Класс Square производный от класса Shape
//1.	Реализует Draw
//b.Содержит список list<Shape*>, заполенный указателями на различные фигуры
//c.С помощью стандартных алгоритмов и адаптеров выводит(Draw) все фигуры
//d.С помощью стандартных алгоритмов и адаптеров сортирует список по положению центра слева - направо(имется в виду, что в начале списка должны идти фигуры находящиеся левее, координата x) и выводит фигуры(Draw)
//e.С помощью стандартных алгоритмов и адаптеров сортирует список по положению центра справа - налево и выводит фигуры
//f.С помощью стандартных алгоритмов и адаптеров сортирует список по положению центра сверху - вниз и выводит фигуры
//g.С помощью стандартных алгоритмов и адаптеров сортирует список по положению центра снизу - вверх и выводит фигуры

#include "stdafx.h"
#define _USE_MATH_DEFINES
#include <vector>
#include <cmath>
#include <iostream>
#include <ctime>
#include <random>
#include "Shape.h"
#include <list>
#include <functional>

#define MAX 5

using namespace std;

struct random{
	float operator()() { return (float)rand() / (float)(RAND_MAX)* MAX; }
 };

struct print {
	void operator()(float i) { cout << i << " "; }
};

int main()
{
	/*vector<float> vec(10);
	vector<float> pi(10, M_PI);
	srand(time(0));

	generate(vec.begin(), vec.end(), random());
	for_each(vec.begin(), vec.end(), print());
	transform(vec.begin(), vec.end(), pi.begin(), vec.begin(), multiplies<float>());
	cout << endl;
	for_each(vec.begin(), vec.end(), print());
	cout << endl;*/

	Circle c(1, 2);
	Triangle t(3, 4);
	Square s(9, 0);

	c.Draw();
	cout << " is leftier than ";
	t.Draw();
	cout << ": " << c.IsMoreLeft(&t) << endl;

	t.Draw();
	cout << " is uppier than ";
	c.Draw();
	cout << ": " << t.IsUpper(&c) << endl;

	vector<Shape*> l = {&c,&s, &t};
	
	for_each(l.begin(), l.end(), mem_fun(&Shape::Draw));

	cout << endl << "From left to right: " << endl;;
	sort(l.begin(), l.end(), mem_fun(&Shape::IsMoreLeft));
	for_each(l.begin(), l.end(), mem_fun(&Shape::Draw));
	
	cout << endl << "From right to left: " << endl;;
	sort(l.begin(), l.end(), not2(mem_fun(&Shape::IsMoreLeft)));
	for_each(l.begin(), l.end(), mem_fun(&Shape::Draw));

	cout << endl << "From top to down: " << endl;;
	sort(l.begin(), l.end(), mem_fun(&Shape::IsUpper));
	for_each(l.begin(), l.end(), mem_fun(&Shape::Draw));

	cout << endl << "From down to top: " << endl;;
	sort(l.begin(), l.end(), not2(mem_fun(&Shape::IsUpper)));
	for_each(l.begin(), l.end(), mem_fun(&Shape::Draw));

    return 0;
}

