#pragma once


#include <iostream>
#include <string>

using namespace std;

class Shape {
protected:
	int x;
	int y;

public:
	//���������� ����������� �� ������ ������ �����(������������ �� ��������� ������) ��� ������ ���������� � �������� ���������
	bool IsMoreLeft(const Shape* f) {
		return x < f->x;
	}

	//���������� ����������� �� ������ ������ ����(������������ �� ��������� ������) ��� ������ ���������� � �������� ���������
	bool IsUpper(const Shape* f) {
		return y > f->y;
	}

	virtual void Draw() = 0;
};

class Circle:public Shape {
private:
	string name = "Circle";
public:
	Circle(int a, int b) { x = a; y = b; }
	void Draw() {
		cout << name << "(" << x << "; " << y << ")";
	}
};

class Triangle:public Shape {
private:
	string name = "Triangle";
public:
	Triangle(int a, int b) { x = a; y = b; }
	void Draw() {
		cout << name << "(" << x << "; " << y << ")";
	}
};

class Square:public Shape {
private:
	string name = "Square";
public:
	Square(int a, int b) { x = a; y = b; }
	void Draw() {
		cout << name << "(" << x << "; " << y << ")";
	}
};

