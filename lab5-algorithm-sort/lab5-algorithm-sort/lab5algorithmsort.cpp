// 1.	Написать программа, которая выполняет следующие действия:

//a.Заполняет vector<DataStruct> структурами DataStruct, при этом key1 и key2, генерируются случайным образом в диапазоне от - 5 до 5, str заполняется из таблицы(таблица содержит 10 произвольных строк, индекс строки генерируется случайным образом)
//b.Выводит полученный вектор на печать
//c.Сортирует вектор следующим образом :
//i.По возрастанию key1
//ii.Если key1 одинаковые, то по возрастанию key2
//iii.Если key1 и key2 одинаковые, то по возрастанию длинны строки str
//d.Выводит полученный вектор на печать

//

//#include "stdafx.h"
#include <string>
#include <vector>
#include <iterator>
#include <random>
#include <iostream>
#include <ctime>
#include <algorithm>

#define N 15

using namespace std;

typedef struct
{
	int       key1;
	int       key2;
	string  str;
} DataStruct;

void Rand(vector<DataStruct> &vec, vector<string> t) {
	srand(time(NULL));
	for (int i = 0; i < N; i++) {
		vec.at(i).key1 = rand() % 10 + (-5);
		vec.at(i).key2 = rand() % 10 + (-5);
		vec.at(i).str = t.at(rand() % 9);
	}
}

void Print(vector<DataStruct> vec) {
	for (int i = 0; i < N; i++) {
		cout << vec.at(i).key1 << " " << vec.at(i).key2 << " " << vec.at(i).str << " " << vec.at(i).str.size() << endl;
	}
}

bool Comp(DataStruct el1, DataStruct el2) {                          //мрмрмрмрмрмрмрмрмрмрмрмрмрмрмрмрмрмрмрмрмрмрмрмрмрмрмрмрмрмрмрмрмрмрае

	if (el1.key1 <	 el2.key1) return 1;

	if (el1.key1 == el2.key1 && el1.key2 < el2.key2) return 1;

	if (el1.key1 == el2.key1 && el1.key2 == el2.key2 && el1.str.size() < el2.str.size()) return 1;

	return 0;
}

int main()
{
	vector<DataStruct> v(N);

	vector<string> table(10, "str");
	for (int i = 0; i < 10; i++) {
		table[i] = table[i].insert(table[i].size(), rand()%5, i + '0');
	}

	Rand(v, table);

	Print(v);

	sort(v.begin(), v.end(), Comp);

	cout << "After sort: " << endl;

	Print(v);

    return 0;
}

