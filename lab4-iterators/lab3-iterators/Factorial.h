#ifndef factorial
#define factorial
#include "MyIterator.h"

class Factorial {
	//friend MyIterator;
private:
	int n[10];
public:

	Factorial() {
		for (int i = 1; i <= 10; i++)
			n[i-1] = i;
	}

	MyIterator<int> begin() {
		return MyIterator<int>(&n[0]);
	}

	MyIterator<int> end() {		
		return MyIterator<int>(&n[10]);
	}

	
};
#endif factorial