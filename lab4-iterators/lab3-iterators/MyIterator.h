#ifndef _MYITERATOR_H_
#define _MYITERATOR_H_
#include <iterator>

template <typename T>
class MyIterator
{
public:
	typedef T value_type;
	typedef T* pointer;
	typedef T& reference;
	typedef std::forward_iterator_tag iterator_category;
	typedef ptrdiff_t difference_type;

	// ������������
	MyIterator();
	MyIterator(T *);

	// ������ ����������
	MyIterator& operator ++();    // ���������� ���������
	MyIterator  operator ++(int); // ����������� ���������

	MyIterator& operator --();    // ���������� ���������
	MyIterator  operator --(int); // ����������� ���������

								  // ��������� ���������
	int operator *();

	// ��������� ���������
	bool operator ==(const MyIterator &);
	bool operator !=(const MyIterator &);

private:
	T* current;
	T fact;
};


template <typename T>
MyIterator<T>::MyIterator() : current(0)
{}

template <typename T>
MyIterator<T>::MyIterator(T* curr) : current(curr)
{}


//���������� ������ ���������� �������� ����� ����������
template <typename T>
MyIterator<T>& MyIterator<T>::operator ++()
{
	++current;
	return *this;
}
//����������� ������ ���������� �������� �� ����������
template <typename T>
MyIterator<T> MyIterator<T>::operator ++(int)
{
	MyIterator tmp(*this);
	operator++();
	return tmp;
}

template <typename T>
MyIterator<T>& MyIterator<T>::operator --()
{
	--current;
	return *this;
}

//����������� ������ ���������� �������� �� ����������
template <typename T>
MyIterator<T> MyIterator<T>::operator --(int)
{
	MyIterator tmp(*this);	
	operator--();
	return tmp;
}

template <typename T>
int MyIterator<T>::operator *()
{
	int a = 1;
	int *f;
	for (int i = 2; i <= *current; i++)
		a *= i;
	f = &a;
	return *f;
}

template <typename T>
bool MyIterator<T>::operator ==(const MyIterator &other)
{
	return current == other.current;
}

template <typename T>
bool MyIterator<T>::operator !=(const MyIterator &other)
{
	return !(*this == other);
}

#endif