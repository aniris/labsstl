//� ������� ����� ���� ��������� ��������, ������� �������� ��� ���������� ������������� ���� �� ���� ������� ����������(low, normal, high)
//�������� �� ������� ����������� � ������������ � �� ������������(������� ����������� �������� � ����������� high, ����� normal, ����� low), �������� � ����������� ������������ ����������� �� ������� � ������� �� �����������.
//� ������� ����� ����� ����������� �������� ����������� � ��� �������� � ����������� low ����������� � ������ ����������� � ������� ����������� ���� ��������� �� high � ��������� �������� � ����������� normal.

#include "stdafx.h"
#include "QueueWithPriority.h"
#include "list"

list <QueueElement> dHigh;
list <QueueElement> dNormal;
list <QueueElement> dLow;

ElementPriority el;

QueueWithPriority::QueueWithPriority()
{
}
QueueWithPriority::~QueueWithPriority()
{
}

// �������� � ������� ������� element � ����������� priority
void QueueWithPriority::PutElementToQueue(const QueueElement & element, ElementPriority priority)
{
	switch (priority)
	{
	case HIGH:
		dHigh.push_back(element);
		break;
	case NORMAL:
		dNormal.push_back(element);
		break;
	case LOW:
		dLow.push_back(element);
		break;
	}
}

// �������� ������� �� �������
// ����� ������ ���������� ������� � ���������� �����������, ������� ���
// �������� � ������� ������ ������
QueueElement QueueWithPriority::GetElementFromQueue()
{
	QueueElement el;

	if (dHigh.size() > 0) {
		el = dHigh.front();
		dHigh.pop_front();
	}
	else if (dNormal.size() > 0) {
		el = dNormal.front();
		dNormal.pop_front();
	}
	else if (dLow.size() > 0) {
		el = dLow.front();
		dLow.pop_front();
	}
	else
		throw "Empty";

	return el;
}

// ��������� �����������
void QueueWithPriority::Accelerate()
{
	dHigh.splice(dHigh.end(), dLow);
}
