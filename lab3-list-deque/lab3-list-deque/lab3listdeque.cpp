// lab3deque.cpp: определяет точку входа для консольного приложения.
//

//#include "stdafx.h"
#include "QueueWithPriority.h"
#include "iostream"
#include "Rand.cpp"

int main()
{
	QueueWithPriority q;
	QueueElement el;
	el.name = "1 - LOW";
	q.PutElementToQueue(el, LOW);
	el.name = "2 - HIGH";
	q.PutElementToQueue(el, HIGH);
	el.name = "3 - HIGH";
	q.PutElementToQueue(el, HIGH);
	el.name = "4 - NORMAL";
	q.PutElementToQueue(el, NORMAL);

	el = q.GetElementFromQueue();
	cout << el.name << endl;

	el = q.GetElementFromQueue();
	cout << el.name << endl;

	el = q.GetElementFromQueue();
	cout << el.name << endl;

	el = q.GetElementFromQueue();
	cout << el.name << endl;

	el.name = "1 - LOW";
	q.PutElementToQueue(el, LOW);
	el.name = "2 - HIGH";
	q.PutElementToQueue(el, HIGH);
	el.name = "3 - HIGH";
	q.PutElementToQueue(el, HIGH);
	el.name = "4 - NORMAL";
	q.PutElementToQueue(el, NORMAL);

	q.Accelerate();

	cout << "After accelerate: " << endl;

	el = q.GetElementFromQueue();
	cout << el.name << endl;

	el = q.GetElementFromQueue();
	cout << el.name << endl;

	el = q.GetElementFromQueue();
	cout << el.name << endl;

	el = q.GetElementFromQueue();
	cout << el.name << endl;

	int n[8] = { 0, 1, 2, 3, 4, 5, 7, 14 };

	for (int i = 0; i < 8; i++) {
		Rand r(n[i]);
		cout << "List " << n[i] << ":" << endl;
		r.Print();
		cout << endl;
		r.PrintTask();
		cout << endl << endl;
	}
	

	return 0;
}

