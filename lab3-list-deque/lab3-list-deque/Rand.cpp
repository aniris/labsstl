#include "stdafx.h"
#include <list>
#include <random>
#include <malloc.h>
#include <iostream>

//��������� list<int> 15 ���������� ���������� �� 1 �� 20, ������ ����� ��������� �� 0 �� 20 ��������(����������� ��������� �� ����� ������ 0, 1, 2, 3, 4, 5, 7, 14)
//������� ���������� ������ � ��������� ������� : ������ �������, ��������� �������, ������ �������, ������������� �������, ������ ������� � �.�.


using namespace std;

class Rand {
	list<int> l;
	//int size;
	int* arr;

public:
	Rand(int n) {
		if (n > 20 || n < 0)
			throw "size : 0 to 20";
		if (n == 0) return;

		arr = (int*)malloc(n * sizeof(int));

		for (int i = 0; i < n; i++) {
			if (i < 15)
				arr[i] = rand() % 20;
			else {
				arr[i] = arr[rand() % 5];
			}

			l.push_back(arr[i]);
		}
	}

	void Print() {
		auto iter = l.begin();
		while (iter != l.end()) {
			cout << *iter << " ";
			iter++;
		}
	}

	void PrintTask() {
		if (l.size() == 0)
			return;

		auto begin = l.begin();
		auto end = l.end();
		
		PrintTask(begin, end, 0);
	}

	void PrintTask(list<int>::iterator begin, list<int>::iterator end, int c) {

		if (c >= (l.size() / 2)) {
			if(l.size() % 2 == 0)
				return;
			end--;
			cout << (*end);
			return;
		}
		c++;
		end--;
		cout << (*begin) << " " << (*end) << " ";
		begin++;
		PrintTask(begin, end, c);
	}
};