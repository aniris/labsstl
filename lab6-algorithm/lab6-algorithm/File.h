#pragma once

#include <string>
#include <fstream>
#include <vector>
#include <algorithm>
#include <regex>
#include <iostream>

using namespace std;

class File {

private:
	string text;
	vector<string> uniqueWords;
	ifstream file;

public:
	File(string path) {
		file.open(path);
		if (!file) return;
			typedef istreambuf_iterator<char> iter;
		string str((iter(file)), iter());
		text = str;
	}

	vector<string> GetUnique() {
		smatch m;
		regex e("[^[:s:]]+");
		//regex e("[a-zA-Z]+");
		string buffer = text;

		while (regex_search(buffer, m, e)) {
			uniqueWords.push_back(m[0].str());
			buffer = m.suffix().str();
		}

		sort(uniqueWords.begin(), uniqueWords.end());
		auto last = unique(uniqueWords.begin(), uniqueWords.end());
		uniqueWords.erase(last, uniqueWords.end());

		return uniqueWords;
	}

	void PrintAllText() {
		cout << text << endl;
	}

	void PrintUniqueWords() {
		GetUnique();
		for (int i = 0; i < uniqueWords.size(); i++)
			cout << uniqueWords[i] << endl;
	}
};