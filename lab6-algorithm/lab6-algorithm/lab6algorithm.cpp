//1.	Написать программу, которая выполняет следующие действия:
//a.Читает содержимое текстового файла
//b.Выделяет слова, словом считаются последовательность символов разделенных пробелами и / или знаками табуляции и / или символами новой строки
//c.Вывести список слов присутствующий в тексте без повторений(имеется в виду, что одно и то же слово может присутствовать в списке только один раз)
//
//2.	Написать программу, которая выполняет следующие действия(все операции должны выполняться с помощью стандартных алгоритмов) :
//	a.Заполняет вектор геометрическими фигурами.Геометрическая фигура может быть треугольником, квадратом, прямоугольником или пяти угольником.Структура описывающая геометрическую фигуру  определена ниже,
//	b.Подсчитывает общее количество вершин всех фигур содержащихся в векторе(так треугольник добавляет к общему числу 3, квадрат 4 и т.д.)
//	c.Подсчитывает количество треугольников, квадратов и прямоугольников в векторе
//	d.Удаляет все пятиугольники
//	e.На основании этого вектора создает vector<Point>, который содержит координаты одной из вершин(любой) каждой фигуры, т.е.первый элемент этого вектора содержит координаты одной из вершину первой фигуры, второй элемент этого вектора содержит координаты одной из вершину второй фигуры и т.д.
//	f.Изменяет вектор так, чтобы он содержал в начале все треугольники, потом все квадраты, а потом прямоугольники.
//	g.Распечатывает вектор после каждого этапа работы

//

//#include "stdafx.h"
#include "File.h"
#include "Shapes.h"

//typedef struct
//{
//	int x, y;
//} Point;
//
//typedef  struct
//{
//	int vertex_num;      // количество вершин, для треугольника 3, для квадрата и 
//						 // прямоугольника 4, для пяти угольника 5
//	vector<Point> vertexes;   // вектор содержащий координаты вершин фигуры
//							  // Для треугольника содержит 3 элемента
//							  // Для квадрата и прямоугольника содержит 4 элемента
//							  // Для пятиугольника 5 элементов
//} Shape;


int main()
{/*
	File f("lab6algorithm.cpp");

	f.PrintAllText();
	cout << endl;
	f.PrintUniqueWords();*/

	/*vector<Point> p;
	p.push_back({ 1, 0 });*/

	Shapes s({ 4,{ { 1,0 },{ 3,2 },{ 3,0 },{ 1,2 } } });
	s.Add({ 4,{ { 2,0 },{ 4,2 },{ 4,0 },{ 1,2 } } });
	s.Add({ 3,{ { 1,0 },{ 3,2 },{ 1,2 } } });
	s.Add({ 5,{ { 1,0 },{ 4,2 },{ 4,0 },{ 1,2 },{ 0,0 } } });
	s.Add({ 4,{ { 1,0 },{ 4,2 },{ 4,0 },{ 1,2 } } });
	s.Add({ 5,{ { 1,0 },{ 4,2 },{ 4,0 },{ 1,2 },{ 0,0 } } });
	
	s.PrintShapes();
	s.DeletePentagones();
	cout << "==========After removal of pentagons==========" << endl;
	s.PrintShapes();
	s.PrintPoints();
	s.Sort();
	cout << "==========After sorting==========" << endl;
	s.PrintShapes();

	return 0;
}

