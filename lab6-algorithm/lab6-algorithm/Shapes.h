//+a.��������� ������ ��������������� ��������.�������������� ������ ����� ���� �������������, ���������, ��������������� ��� ���� ����������.��������� ����������� �������������� ������ ���������� ����,
//+b.������������ ����� ���������� ������ ���� ����� ������������ � �������(��� ����������� ��������� � ������ ����� 3, ������� 4 � �.�.)
//+c.������������ ���������� �������������, ��������� � ��������������� � �������
//+d.������� ��� �������������
//+e.�� ��������� ����� ������� ������� vector<Point>, ������� �������� ���������� ����� �� ������(�����) ������ ������, �.�.������ ������� ����� ������� �������� ���������� ����� �� ������� ������ ������, ������ ������� ����� ������� �������� ���������� ����� �� ������� ������ ������ � �.�.
//+f.�������� ������ ���, ����� �� �������� � ������ ��� ������������, ����� ��� ��������, � ����� ��������������.
//+g.������������� ������ ����� ������� ����� ������

#pragma once
#include <vector>
#include <iostream>
#include <iterator>
#include <algorithm>

using namespace std;

typedef struct
{
	int x, y;
} Point;

typedef  struct
{
	int vertex_num;      // ���������� ������, ��� ������������ 3, ��� �������� � 
						 // �������������� 4, ��� ���� ��������� 5
	vector<Point> vertexes;   // ������ ���������� ���������� ������ ������
							  // ��� ������������ �������� 3 ��������
							  // ��� �������� � �������������� �������� 4 ��������
							  // ��� ������������� 5 ���������
} Shape;



enum Type{
	Triangle, Square, Rectangle, Pentagon
};

class Shapes {
private:
	vector<Shape> sh;
	vector<Point> vertexes;
	int countVertexes = 0;
	int countTriangle = 0;
	int countRectangle = 0;
	int countSquare = 0;

	static Type getTypeShape(Shape el)  {
		switch (el.vertex_num)
		{
		case 3:
			return Triangle;
		case 5:
			return Pentagon;
		case 4: // ���� ������ 4, �� ���������� �����
			float side1 = sqrt(pow((el.vertexes[1].x - el.vertexes[0].x), 2) + pow((el.vertexes[1].y - el.vertexes[0].y), 2)); // ��������� 3 �����
			float side2 = sqrt(pow((el.vertexes[2].x - el.vertexes[0].x), 2) + pow((el.vertexes[2].y - el.vertexes[0].y), 2));
			float side3 = sqrt(pow((el.vertexes[3].x - el.vertexes[0].x), 2) + pow((el.vertexes[3].y - el.vertexes[0].y), 2));

			if (side1 == side2 || side1 == side3 || side2 == side3) // ���� ���� �� ��� �� ��� �����
				return Square; // �������
			else
				return Rectangle; // �������������
		}
	}

	void GetPoints() {
		for (auto shape : sh) {
			vertexes.push_back((shape).vertexes[0]);
		}
	}

	static bool SortTypes(Shape a, Shape b) {
		if (getTypeShape(a) < getTypeShape(b)) return 1;
		return 0;
	}

public:

	Shapes(Shape el) {
		sh.push_back(el);
		countVertexes = el.vertex_num;
		switch (getTypeShape(el)) {
		case Triangle:
			countTriangle++;
			break;
		case Square:
			countSquare++;
			break;
		case Rectangle:
			countRectangle++;
			break;
		}
	}

	void Add(Shape el) {
		sh.push_back(el);
		countVertexes += el.vertex_num;
		switch (getTypeShape(el)) {
		case Triangle:
			countTriangle++;
			break;
		case Square:
			countSquare++;
			break;
		case Rectangle:
			countRectangle++;
			break;
		}
	}

	void PrintShapes() {
		for (auto shape = sh.begin(); shape < sh.end(); shape++) {
			switch (getTypeShape(*shape)) {
			case Triangle:
				cout << "Triangle" << " ";
				break;
			case Square:
				cout << "Square" << " ";
				break;
			case Rectangle:
				cout << "Rectangle" << " ";
				break;
			case Pentagon:
				cout << "Pentagon" << " ";
				break;
			}
			cout << (*shape).vertex_num << " ";
			for(auto v = (*shape).vertexes.begin(); v < (*shape).vertexes.end(); v++)
				cout << "(" << (*v).x << ", " << (*v).y << ") ";
			cout << endl;
		}
		cout << "Triangles: " << countTriangle << endl;
		cout << "Squares: " << countSquare << endl;
		cout << "Rectangles: " << countRectangle << endl;
		cout << "Vertexes: " << countVertexes << endl;
	}

	void DeletePentagones() {
		for (auto shape = sh.begin(); shape < sh.end();) {
			if (getTypeShape(*shape) == Pentagon) {
				shape = sh.erase(shape);
				countVertexes -= 5;
			}
			else
				shape++;
		}
	}

	void PrintPoints() {
		GetPoints();
		for (auto v : vertexes) {
			cout << "(" << v.x << ", " << v.y << ") ";
		}
		cout << endl;
	}

	void Sort() {
		sort(sh.begin(), sh.end(), SortTypes);
	}
};