//��������� �����, 23531/21-2
//07.09.2017
//
//�������� �������� ���������� (����� ����������) ����������� ������� ����� �����, ��������� ����� at().

#include "stdafx.h"
#include "vector"
#include "iostream"
#include "MyVector.h"

using namespace std;

template <class T>
class At : public MyVector<T> {
public:

	At(T * arr, int size) {
		vector<T> vec1(&arr[0], &arr[size]);
		vec = vec1;
	}

	At(vector<T> vec1) {
		vec = vec1;
	}

	void Sorting() {
		T buf;

		for (int i = 0; i < vec.size(); i++) {
			for (int j = i; j < vec.size(); j++) {
				if (vec.at(i) < vec.at(j)) {
					buf = vec.at(i);
					vec.at(i) = vec.at(j);
					vec.at(j) = buf;
				}
			}
		}
	}
};