//��������� �����, 23531/21-2
//07.09.2017
//
//�������� �������� ���������� (����� ����������) ����������� ������� ����� �����, ��������� ��� ������� � ����������� ������� ������ ���������. ��� ������ � ����������� ��������� ������������ ������ ��������� ��������� �������� �������� � �������� � ���������� (���������, ����� ��������� ����� ��������� ������������ �� ��������� �������). 

#include "stdafx.h"
#include "vector"
#include "iostream"

using namespace std;

class Iterator {
public:
	vector<int> vec;

	Iterator(vector <int> v) {
		vec = v;
	}

	void Sorting() {
		int buf;
		vector<int>::iterator current, prev;

		for (int j = 0; j < vec.size() - 1; j++) {
			current = vec.begin();
			prev = vec.begin();

			current++;
			while(current != vec.end() ) {

				if (*prev < *current) {
					buf = *prev;
					*prev = *current;
					*current = buf;
				}

				current++;
				prev++;
			}
		}

	}

	void Print() {
		vector<int>::iterator iter = vec.begin();

		while (iter != vec.end()) {
			cout << *iter << " ";
			iter++;
		}

		cout << endl;
	}
};