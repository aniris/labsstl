#ifndef myvector_h
#define myvector_h

#include "stdafx.h"
#include "vector"
#include "iostream"

using namespace std;

template <typename T>
class MyVector {
protected:
	vector <T> vec;

public:

	MyVector() {};

	MyVector(T * arr, int size) {
		vector<T> vec1(&arr[0], &arr[size]);
		vec = vec1;
	}

	MyVector(vector<T> vec1) {
		vec = vec1;
	}

	void Print() {
		for (auto iter = vec.begin(); iter < vec.end(); iter++) {
			cout << *iter << " ";
		}
		cout << endl;
	}
};

#endif myvector_h