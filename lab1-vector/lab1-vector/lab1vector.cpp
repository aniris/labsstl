//Соловьева Ирина, 23531/21-2
//07.09.2017
//
//lab1-vector-task

#include "stdafx.h"
#include "vector"
#include "iostream"
#include "Brackets.cpp"
#include "At.cpp"
#include "Iterator.cpp"
#include "File.cpp"
#include "Read.cpp"
#include "stdlib.h"
#include "ctime"
#include "algorithm"

#define A -1.0
#define B 1.0

using namespace std;

void fillRandom(double* arr, int size) {
	for (int i = 0; i < size; i++)
		arr[i] = (double)rand() / (double)RAND_MAX * (B - A) + A;
}

bool compare(int i1, int i2)
{
	return (i1< i2);
}

int main()
{
	vector<int> v{1,2,4,5};
	vector<double> vRandom(10);


	Brackets<int> v1(v);
	cout << "Brackets:" << endl << "Before sorting: ";
	v1.Print();
	v1.Sorting();
	cout << "After sorting: ";
	v1.Print();

	/*At v2(v);
	cout << "At:" << endl << "Before sorting: ";
	v2.Print();
	v2.Sorting();
	cout << "After sorting: ";
	v2.Print();*/

	//Iterator v3(v);
	//cout << "Iterator:" << endl << "Before sorting: ";
	//v3.Print();
	/*unsigned int start_time = clock();
	v3.Sorting();
	unsigned int end_time = clock();
	unsigned int search_time = end_time - start_time;
	cout << search_time << endl;
	*//*Iterator v33(v);
	unsigned int start_time = clock();
	sort(v33.vec.begin(), v33.vec.end(), compare);
	unsigned int end_time = clock();
	unsigned int search_time  = end_time - start_time;
	cout << search_time << endl;*/

	//cout << "After sorting: ";
	//v3.Print();

	/*File v4("file.txt");
	v4.PrintArray();
	v4.ArrayToVector();
	v4.PrintVector();*/

	/*Read v5;
	v5.ReadNumbers();
	v5.Print();
	v5.Format();
	v5.Print();*/

	/*fillRandom(&vRandom[0], vRandom.size());
	Brackets<double> v6(vRandom);
	v6.Print();
*/
	return 0;
}

