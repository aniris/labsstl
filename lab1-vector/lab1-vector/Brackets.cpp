//��������� �����, 23531/21-2
//07.09.2017
//
//�������� �������� ����������(����� ����������) ����������� ������� ����� �����, ��������� �������� operator[].

#include "stdafx.h"
#include "vector"
#include "iostream"
#include "MyVector.h"

using namespace std;


template <typename T>
class Brackets: public MyVector<T>{
public:

	Brackets (T * arr, int size) {
		vector<T> vec1(&arr[0], &arr[size]);
		vec = vec1;
	}

	Brackets (vector<T> vec1) {
		vec = vec1;
	}

	void Sorting() {
		T buf;

		for (int i = 0; i < vec.size(); i++) {
			for (int j = i; j < vec.size(); j++) {
				if (vec[i] < vec[j]) {
					buf = vec[i];
					vec[i] = vec[j];
					vec[j] = buf;
				}
			}
		}

	}

	/*void Print() {
		base::Print();
	}*/
};